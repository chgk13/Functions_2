def text_to_num(text: str, num: int, digit_map: dict, negative):
    if len(text) == 0:
        num *= (-1)**negative
        return 3*num + 1 if num % 2 else num//2
    digit = digit_map[text[0]]
    return text_to_num(text[1:], 10*num + digit, digit_map, negative)


def main():
    digit_map = {
        chr(a): b for a, b in zip(range(ord('0'), ord('0')+11), range(10))}
    while True:
        string = input('-> ')
        if string.isdecimal():
            print(text_to_num(string, 0, digit_map, False))
        elif string == 'cancel':
            print('Bye!')
            break
        elif len(string) > 0 and string[0] == '-' and string[1:].isdigit():
            print(text_to_num(string[1:], 0, digit_map, True))
        else:
            print('Не удалось преобразовать введенный текст в число.')


if __name__ == '__main__':
    main()
