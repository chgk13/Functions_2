def f(x):
    return x ** 2


def f_prime(x):
    return 2 * x


def mysqrt(number, accuracy=None, _x_1=0):
    if number <= 0:
        raise ValueError('A number must be positive.')
    if number == 1:
        return 1
    if number > 1:
        accuracy = accuracy or 1e-5
    else:
        accuracy = number if not accuracy else accuracy
    x_1 = _x_1 or number
    x_1 = x_1 - ((f(x_1) - number)/f_prime(x_1))
    if abs(f(x_1) - number) < accuracy**2:
        str_accuracy = str(accuracy)
        if 'e' in str_accuracy:
            rounder = int(str_accuracy[:2:-1][::-1])
        else:
            rounder = len(str_accuracy) - 2
        return round(x_1, ndigits=rounder)
    return mysqrt(number, accuracy, x_1)


def test():
    assert mysqrt(4) == 2.0
    assert mysqrt(5, 0.01) == 2.24
    assert mysqrt(1) == 1.0
    assert mysqrt(4e-2) == 0.2
    assert mysqrt(4e-4) == 0.02
    assert mysqrt(4e-6) == 0.002
    assert mysqrt(4e-8) == 0.0002
    assert mysqrt(4e-16) == 2e-8
    assert mysqrt(5e-16, 0.001) == 0.001


if __name__ == '__main__':
    test()
