def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a


def is_triangle(a, b, c):
    return (a[0] - b[0]) * (b[1] - c[1]) - (a[1] - b[1]) * (b[0] - c[0])


def count_points_on_the_line(a, b):
    if a[0] == b[0]:
        return abs(a[1] - b[1]) + 1
    if a[1] == b[1]:
        return abs(a[0] - b[0]) + 1
    delta_y = abs(a[1] - b[1])
    delta_x = abs(a[0] - b[0])
    g_c_d = gcd(delta_y, delta_x)
    int_slope = (delta_y // g_c_d)
    return (delta_y // int_slope or 1) + 1


def count_points(a, b, c) -> int:
    if not is_triangle(a, b, c):
        raise Exception('It is not a triangle!')
    tri = [a, b, c, a]
    area, border_count = 0, 0
    for i in range(3):
        border_count += count_points_on_the_line(tri[i], tri[i+1])
        area += (tri[i+1][0] + tri[i][0])*(tri[i+1][1] - tri[i][1])/2
    area = abs(area)
    border_count -= 3
    return round(area + 1 + border_count/2)


print(count_points((2, 1), (5, 5), (7, 3)))
print(count_points((-2, -5), (0, 0), (5, 2)))
print(count_points((5, 2), (0, 0), (-2, -5)))
print(count_points((5, 2), (-2, -5), (0, 0)))
print(count_points((-2, -2), (0, 4), (4, 3)))
print(count_points((-2, -2), (0, 4), (3, 5)))
print(count_points((4, -1), (6, 3), (0, -5)))
