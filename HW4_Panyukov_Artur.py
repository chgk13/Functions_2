import functools
import time


def make_cache(save_time=1):
    def decorator(func):
        name = func.__name__ + '_cache'
        globals()[name] = {}

        @functools.wraps(func)
        def wrapper(*args):
            if args in globals()[name]:
                if time.time() - globals()[name][args][1] > save_time:
                    return globals()[name].pop(args)[0]
                return globals()[name][args][0]
            else:
                val = func(*args)
                globals()[name][args] = (val, time.time())
                return val
        return wrapper
    return decorator


@make_cache(1e-3)
def fib(n):
    if n < 2:
        return n
    return fib(n-1) + fib(n-2)


print(fib(300))
print(fib(500))
