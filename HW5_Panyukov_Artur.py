import time
import functools
import sys


sys.setrecursionlimit = 10000


def make_cache(function):
    cache = {}

    @functools.wraps(function)
    def wrapper(*args):
        if args in cache:
            return cache[args]
        else:
            val = function(*args)
            cache[args] = val
            return val
    return wrapper


def timeit(func=None, *, var_name='timings'):
    if func is None:
        return lambda func: timeit(func, var_name=var_name)

    @functools.wraps(func)
    def timed_func(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        stop = time.time()
        prev = globals().get(var_name, [0, 0])
        update = [stop - start, 1]
        globals()[var_name] = list(map(lambda x, y: x + y, prev, update))
        return result
    return timed_func


@timeit(var_name='fib1_timings')
@make_cache
def fib1(n):
    if n < 2:
        return n
    return fib1(n-1) + fib1(n-2)


@timeit(var_name='fib2_timings')
def fib2(n):
    if n < 2:
        return n
    a, b = 1, 1
    for _ in range(n-2):
        a, b = b, a+b
    return b


@timeit(var_name='fib3_timings')
def fib3(n):
    '''
    Accurate until n = 70
    '''
    phi = (1 + 5**(1/2))/2
    return round(phi**n / 5**(1/2))


def matrix_mul(m1, m2):
    return [[
        m1[0][0]*m2[0][0] + m1[0][1]*m2[1][0],
        m1[0][0]*m2[0][1] + m1[0][1]*m2[1][1]
    ], [
        m1[1][0]*m2[0][0] + m1[1][1]*m2[1][0],
        m1[1][0]*m2[0][1] + m1[1][1]*m2[1][1]
    ]]


def matrix_pow(matrix, n):
    if n == 1:
        return matrix
    res = matrix_pow(matrix_mul(matrix, matrix), n//2)
    if n % 2:
        res = matrix_mul(res, matrix)
    return res


@timeit(var_name='fib4_timings')
def fib4(n):
    if n < 2:
        return n
    matrix = [[1, 1], [1, 0]]
    return matrix_pow(matrix, n)[0][1]


def main():
    max_num = 1001

    fib = [fib1, fib2, fib3, fib4]
    fib_dict = {fib1: 'recursion + cache',
                fib2: 'Iter', fib3: 'Phi', fib4: 'matrix'}
    fib_set = {fib2(n) for n in range(1, max_num, 150)}
    globals()['fib2_timings'] = [0, 0]

    for func in fib:
        print(f' This is {fib_dict[func]} :')
        for n in range(1, max_num, 150):
            if globals().get(func.__name__+'_timings', [0])[0] == float('inf'):
                continue
            try:
                res = func(n)
                print(
                    f'{n}: Combined time - {globals()[func.__name__+"_timings"][0]}'
                )
                if res not in fib_set:
                    print(
                        f'{res} is not Fibonacci number. Function disqualified.')
                    globals()[func.__name__+'_timings'][0] = float('inf')
            except RecursionError:
                print(
                    f'Something wrong, function {func.__name__} disqualified.')
                globals()[func.__name__+'_timings'][0] = float('inf')

    result = [(func, globals()[func.__name__+'_timings'][0])
              for func in fib]
    result.sort(key=lambda x: x[1])
    for func, time in result:
        print(f'{fib_dict[func]}: {time}')
    print(f'Winner is {fib_dict[result[0][0]]}!')


if __name__ == '__main__':
    main()
